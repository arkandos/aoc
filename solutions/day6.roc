app "hello"
    packages { pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br" }
    imports [pf.Stdout, pf.Task, "../inputs/day6.txt" as input : Str]
    provides [main] to pf


solve = \games ->
    acc, { time, distance } <- games |> List.walk 1
    # dist btn := btn(time - btn) = -btn^2 + btn*time
    # dist btn = distance + 1                    | solve for btn
    # -btn^2 + btn*time = distance + 1           | we need to travel at least 1mm further
    # -btn^2 + btn*time - (highscore + 1) = 0    | quadradic formula
    a = -1
    b = Num.toF64 time
    c = -(Num.toF64 distance + 1)
    x1 = (-b + Num.sqrt(b*b - 4*a*c)) / (2*a)
    x2 = (-b - Num.sqrt(b*b - 4*a*c)) / (2*a)

    min = Num.ceiling (Num.min x1 x2)
    max = Num.floor (Num.max x1 x2)

    acc * (max - min + 1)


main =
    part1 =
        parseLine = \str ->
            { after: dataStr } <- Str.splitFirst str ":" |> Result.try
            dataStr
                |> Str.split " "
                |> List.dropIf Str.isEmpty
                |> List.mapTry Str.toNat
                
        input
            |> parseInput parseLine
            |> unwrap
            |> solve

    part2 =
        parseLine = \str ->
            { after: dataStr } <- Str.splitFirst str ":" |> Result.try
            
            dataStr
                |> Str.replaceEach " " ""
                |> Str.toNat
                |> Result.map List.single

        input
            |> parseInput parseLine
            |> unwrap
            |> solve

    _ <- Stdout.line "Part 1: \(Num.toStr part1)" |> Task.await
    _ <- Stdout.line "Part 2: \(Num.toStr part2)" |> Task.await
    
    Task.ok {}


unwrap = \result ->
    when result is
        Ok value -> value
        Err _ -> crash "bad unwrap"


parseInput = \str, parseLine ->
    when str |> Str.trim |> Str.split "\n" is
        [timeStr, distStr] ->
            times <- parseLine timeStr |> Result.try
            distances <- parseLine distStr |> Result.try

            zipped =
                time, distance <- List.map2 times distances
                { time, distance }

            Ok zipped

        _ ->
            Err InvalidInput

